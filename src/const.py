# coding=utf-8
from enum import Enum

MAX_MOVES = 10
GAME_OVER = -1
WIDTH = 30
HEIGHT = 30

LEFT_BORDER = - WIDTH // 2
RIGHT_BORDER = WIDTH // 2
BOTTOM_BORDER = - HEIGHT // 2
TOP_BORDER = HEIGHT // 2

BASE_LENGTH = 3

CHOICES = {"RIGHT", "LEFT", "FORWARD"}


class TurnDirection(Enum):
    forward = "FORWARD"
    right = "RIGHT"
    left = "LEFT"


MOVE_SCORE = 1
FOOD_SCORE = 10
HEAD_SCORE = 1
MODULE_SCORE = 1
# сейчас элементов достаточно мало, не думаю, что есть смысл писать генерацию
ENLARGE = [1, 1, 2, 3, 5, 8, 13, 21]
