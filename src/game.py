# coding=utf-8
import json
import math
from random import choices
from typing import Dict, Tuple, List, Set, Any

import const
from snake import Snake


class Game:
    def __init__(self):
        self.width = const.WIDTH
        self.height = const.HEIGHT

        self.snakes: Dict[str, Snake] = {}
        self.food: Set[Tuple[int, int]] = set()

        self.counter: int = 0

    @property
    def scores(self) -> dict:
        return {uid: snake.score for uid, snake in self.snakes.items()}

    @property
    def is_finished(self) -> bool:
        return self.counter >= const.MAX_MOVES or (
                all([snake.dead for snake in self.snakes.values()])
                and
                len(self.snakes) > 0
        )

    @property
    def state(self) -> Dict[str, Any]:
        snakes = {}
        result = {}

        for user_id, snake in self.snakes.items():
            snakes[user_id] = snake.state

        for me, snake in self.snakes.items():
            result[me] = json.dumps({
                    "me": snakes[me],
                    "food": list(self.food),
                    "players": {
                        key: snakes[key] for key in snakes if key != me
                    },
                } if not snake.dead else const.GAME_OVER)

        return result

    def make_food(self, i: int) -> None:
        if i <= 0:
            return

        all_spots: Set[Tuple[int, int]] = {(i, j) for i in range(self.width) for j in range(self.height)}

        for snake in self.snakes.values():
            all_spots.discard(snake.head)
            all_spots.difference_update(snake.body)

        all_spots.difference_update(self.food)

        while i > 0 and len(all_spots) > 0:
            elements: Set[Tuple[int, int]] = set(choices([*all_spots], k=i))  # могут повторяться

            all_spots.difference_update(elements)
            self.food.update(elements)

            i -= len(elements)

    def make_moves(self, moves: Dict[str, str]):
        self.counter += 1

        # все живые змейки делают ход
        for uid, snake in self.snakes.items():
            if snake.dead:
                continue

            snake.change_direction(moves[uid])

        # все живые змейки двигаются
        for snake in self.snakes.values():
            if snake.dead:
                continue

            snake.slither()

        # все змейки проверяются на столкновения
        idx = 0
        for left, snake in self.snakes.items():
            snake.bump_self()
            snake.bump_borders()

            acc = idx = idx + 1
            for right, other in self.snakes.items():
                if acc > 0:
                    acc -= 1
                    continue

                snake.bump_other(other)

        for snake in self.snakes.values():
            snake.check_food(self.food)

        self.make_food(len(self.snakes) - len(self.food))

    def start(self, data: Dict[str, List[str]]) -> None:
        def positions_gen(up_to: int):
            step = 2 * math.pi / up_to
            angle = 0
            while angle <= 2 * math.pi:
                yield (
                    round(math.cos(angle) * const.WIDTH * 1 / 3),
                    round(math.sin(angle) * const.HEIGHT * 1 / 3),
                )

                angle += step
            return

        self.snakes = {
            key: Snake(pos) for key, pos in zip(data["users"], positions_gen(len(data["users"])))
        }
