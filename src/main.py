import json
from typing import List, Dict, Any

from game import Game


class Controller:
    def __init__(self):
        self.game: Game = Game()
        self.finished = False

        self.handlers = {
            "start": self.start,
            "move": self.move,
            "end": self.set_finish
        }

    @staticmethod
    def read_data() -> dict:
        return json.loads(input())

    @staticmethod
    def write_data(msg_type: str, data: dict) -> None:
        msg = {
            "type": msg_type,
            "data": data
        }
        print(json.dumps(msg))

    def process(self, msg: dict) -> Any:
        return self.handlers[msg["type"]](msg["data"])

    def run(self) -> None:
        while not self.game.is_finished and not self.finished:
            msg = self.read_data()
            self.process(msg)
        self.finish()

    def start(self, data: Dict[str, List[str]]) -> None:
        self.game.start(data)
        self.write_data("status", self.game.state)

    def move(self, data: dict) -> None:
        self.game.make_moves(data)
        self.write_data("status", self.game.state)

    def set_finish(self, data: dict) -> None:
        self.finished = True

    def finish(self) -> None:
        data = self.game.scores
        self.write_data("end", data)


if __name__ == "__main__":
    controller = Controller()
    controller.run()
