# coding=utf-8
from typing import Tuple, List, Set, Any, Dict, Union

from const import TurnDirection, BASE_LENGTH, LEFT_BORDER, RIGHT_BORDER, TOP_BORDER, \
    BOTTOM_BORDER, MOVE_SCORE, FOOD_SCORE, HEAD_SCORE, MODULE_SCORE, ENLARGE


class Snake:
    def __init__(self, pos: Tuple[int, int]):
        self.length = BASE_LENGTH  # нелинейно!
        self.buffer = 0

        self.head: Tuple[int, int] = pos  # [x, y]
        self.body: List[Tuple[int, int]] = []

        self.direction: Tuple[int, int] = (0, 1)  # velocity - ->x ^y

        self.score = 0
        self.dead = False

    @property
    def state(self) -> Union[Dict[str, Any], int]:
        return {
            "length": self.length,
            "head": self.head,
            "body": self.body,
            "score": self.score,
            "dead": self.dead,
        }

    @staticmethod
    def fibonacci():
        pass

    def change_direction(self, direction: str) -> None:
        # неверный ввод
        try:
            direction: TurnDirection = TurnDirection(direction)
        except ValueError:
            self.dead = True
            return

        changes = {
            TurnDirection.forward: self.direction,
            TurnDirection.right: (self.direction[1], -self.direction[0]),
            TurnDirection.left: (-self.direction[1], self.direction[0]),
        }

        self.direction = changes[direction]

    def check_food(self, food: Set[Tuple[int, int]]) -> None:
        if self.head in food:
            self._enlarge()
            food.remove(self.head)
            self.score += FOOD_SCORE

    # Числа Фибоначчи
    def _enlarge(self):
        self.buffer += 1
        # BASE_LENGTH + 1 = начальная длина змейки
        # Мы сравниваем буфер с элементом списка с числами Фибоначчи
        # 0 элемент - для 1 удлиннения, 1 - для второго и т.д.
        if self.buffer == ENLARGE[self.length - BASE_LENGTH - 1]:
            self.buffer = 0
            self.length += 1
        else:
            pass

    def slither(self) -> None:
        self.body.insert(0, self.head)

        self.head = (
            self.head[0] + self.direction[0],
            self.head[1] + self.direction[1],
        )

        self.body = self.body[:self.length]

        self.score += MOVE_SCORE

    def bump_self(self) -> None:
        if self.dead:
            return
        if self.head in self.body:
            self.dead = True

    def bump_borders(self) -> None:
        if RIGHT_BORDER < self.head[0] < LEFT_BORDER \
                or TOP_BORDER < self.head[1] < BOTTOM_BORDER:
            self.dead = True

    def bump_other(self, other: "Snake"):
        if self is other:
            return

        if self.head in other.body:
            other.score = HEAD_SCORE + MODULE_SCORE * self.length
            self.dead = True

        if other.head in self.body:
            self.score = HEAD_SCORE + MODULE_SCORE * other.length
            other.dead = True

        if self.head == other.head:
            if self.length > other.length:
                other.dead = True
            elif other.length > self.length:
                self.dead = True
            else:
                self.dead = other.dead = True
