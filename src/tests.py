from random import choice
from const import CHOICES

def random_usernames(cap_of_users, name_len):
    # рандомная генерация имени
    lst_of_users = []
    while len(lst_of_users) < cap_of_users:
        lst_of_users.append(''.join(choice(string.ascii_lowercase) for i in range(name_len)))
    return lst_of_users

def random_choices(list_of_users: list) -> dict:
    choices = []
    for i in range(len(list_of_users)):
        choices.append(choice(CHOICES))
    data = dict(zip(list_of_users, choices))
    return data

def end_data():
    msg = {
        "type": "end",
        "data": {}
    }
    return msg